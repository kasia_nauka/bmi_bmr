import resources.bmi_calculator as bmi_calculator
import resources.bmr_calculator as bmr_calc

wzorst = int(input("Podaj swoj wzrost: "))
waga = int(input("Podaj swoja wage "))
wiek = int(input("Podaj swoj wiek: "))
plec = str(input("Podaj swoja plec:(man/woman) "))
while plec != 'man' and plec != 'woman':
    plec = str(input("Podaj swoja plec:(man/woman) "))


bmi = bmi_calculator.bmi_calculator(weight=waga, height=wzorst)
bmr = bmr_calc.bmr_calculator(weight=waga, height=wzorst, age=wiek, sex=plec)


print("BMI: "+ str(bmi))
print("BMR: "+ str(bmr))



