def bmr_calculator(weight, height, age, sex):
    if sex == "man":
        bmr_height = 5*int(height)
        bmr_weight = 13.7*int(weight)
        bmr_age = 6.76*int(age)
        bmr = 66 + float(bmr_height) + float(bmr_weight) - float(bmr_age)
        return bmr
    else:
        bmr_height = 1.8*int(height)
        bmr_weight = 9.6*int(weight)
        bmr_age = 4.7*int(age)
        bmr = 655 + float(bmr_height) + float(bmr_weight) - float(bmr_age)
        return bmr
